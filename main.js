const express = require("express");
const app = express();
const PORT = 3000;

// build small Rest API with Express
console.log("Server-side program starting...");

// baseurl: http://localhost:3000
// Endpoint: http://localhost:3000
app.get('/', (reg, res) => {
    res.send('Hello world');
});

/**
 * This arrow function adds two numbers together
 * @param {Number} a first param
 * @param {Number} b second param
 * @returns {Number}
 */
const add = (a, b) => {
    const sum = a + b;
    return sum;
};

app.get('/add', (req, res) => {
    console.log(req.query);
    const sum = add(Number(req.query.a), Number(req.query.b));
    res.send(sum.toString());
});


app.listen(PORT, () => console.log(
    `Server listening http://localhost:${PORT}`
));